# Text to Base64

Convert text to base64 (and vice versa) using [PyScript](https://github.com/pyscript/pyscript)

![screenshot](screenshots/main_window.png)
