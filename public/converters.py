from base64 import b64encode, b64decode

plaintext_area = Element("plaintext")
base64text_area = Element("base64text")

def plain_to_base64(*args, **kwargs):
    message = plaintext_area.element.value
    if not message:
        return
    base64text_area.element.value = b64encode(message.encode()).decode()


def base64_to_plain(*args, **kwargs):
    message = base64text_area.element.value
    if not message:
        return
    plaintext_area.element.value = b64decode(message.encode()).decode()
